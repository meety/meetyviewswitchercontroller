//
//  MeetyAppDelegate.h
//  BasicUse
//
//  Created by 최건우 on 12. 9. 28..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetyAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
