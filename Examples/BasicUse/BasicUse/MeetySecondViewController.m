//
//  MeetySecondViewController.m
//  BasicUse
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetySecondViewController.h"
#import "MeetyViewSwitcherController.h"

@interface MeetySecondViewController ()

@end

@implementation MeetySecondViewController
@synthesize button=_button;
@synthesize animatedButton=_animatedButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    CGRect frame = self.animatedButton.frame;
    frame.origin.y = self.button.frame.origin.y + self.button.frame.size.height;
    self.animatedButton.frame = frame;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        NSLog(@"Second : Portrait");
    } else{
        NSLog(@"Second : Landscape");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
    NSLog(@"Second : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"Second : %@",NSStringFromSelector(_cmd));
}

- (void)viewWillDisappear:(BOOL)animated{
    NSLog(@"Second : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidDisappear:(BOOL)animated{
    NSLog(@"Second : %@",NSStringFromSelector(_cmd));
}

#pragma mark - Actions
- (IBAction)moveToThird:(id)sender{
    [self.viewSwitcherController setPage:2 animated:NO];
}
- (IBAction)moveToThirdAnimated:(id)sender{
    [self.viewSwitcherController setPage:2 animated:YES];
}

@end
