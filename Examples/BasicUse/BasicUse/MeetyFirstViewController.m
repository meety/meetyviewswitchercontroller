//
//  MeetyFirstViewController.m
//  BasicUse
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetyFirstViewController.h"

@interface MeetyFirstViewController ()

@end

@implementation MeetyFirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        NSLog(@"First : Portrait");
    } else{
        NSLog(@"First : Landscape");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"First : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"First : %@",NSStringFromSelector(_cmd));
}

- (void)viewWillDisappear:(BOOL)animated{
    NSLog(@"First : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidDisappear:(BOOL)animated{
    NSLog(@"First : %@",NSStringFromSelector(_cmd));
}

@end