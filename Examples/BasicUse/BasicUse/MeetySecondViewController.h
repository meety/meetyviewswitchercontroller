//
//  MeetySecondViewController.h
//  BasicUse
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetySecondViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton* button;
@property (nonatomic, strong) IBOutlet UIButton* animatedButton;

- (IBAction)moveToThird:(id)sender;
- (IBAction)moveToThirdAnimated:(id)sender;

@end
