//
//  MeetyAppDelegate.m
//  BasicUse
//
//  Created by 최건우 on 12. 9. 28..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetyAppDelegate.h"
#import "MeetyViewSwitcherController.h"
#import "MeetyFirstViewController.h"
#import "MeetySecondViewController.h"
#import "MeetyThirdViewController.h"

@implementation MeetyAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Initialize view switcher
    MeetyFirstViewController* firstViewController = [[MeetyFirstViewController alloc] initWithNibName:@"MeetyFirstViewController" bundle:nil];
    MeetySecondViewController* secondViewController = [[MeetySecondViewController alloc] initWithNibName:@"MeetySecondViewController" bundle:nil];
    MeetyThirdViewController* thirdViewController = [[MeetyThirdViewController alloc] initWithNibName:@"MeetyThirdViewController" bundle:nil];
    MeetyViewSwitcherController* viewSwitcherController = [[MeetyViewSwitcherController alloc] initWithFirstViewController:firstViewController];
    [viewSwitcherController addViewController:secondViewController];
    [viewSwitcherController addViewController:thirdViewController];
    
    // Background
    UIImage* image = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"main_back" ofType:@"png"]];
    viewSwitcherController.portraitBackgroundImage = image;
    viewSwitcherController.landscapeBackgroundImage = image;
    
    viewSwitcherController.contentInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    
    self.window.rootViewController = viewSwitcherController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application{
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
}

- (void)applicationWillTerminate:(UIApplication *)application{
}

@end
