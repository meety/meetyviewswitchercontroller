//
//  MeetyThirdViewController.m
//  BasicUse
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetyThirdViewController.h"

@interface MeetyThirdViewController ()

@end

@implementation MeetyThirdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        NSLog(@"Third : Portrait");
    } else{
        NSLog(@"Third : Landscape");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"Third : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"Third : %@",NSStringFromSelector(_cmd));
}

- (void)viewWillDisappear:(BOOL)animated{
    NSLog(@"Third : %@",NSStringFromSelector(_cmd));
}

- (void)viewDidDisappear:(BOOL)animated{
    NSLog(@"Third : %@",NSStringFromSelector(_cmd));
}

@end
