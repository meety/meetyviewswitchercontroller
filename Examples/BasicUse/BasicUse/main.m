//
//  main.m
//  BasicUse
//
//  Created by 최건우 on 12. 9. 28..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MeetyAppDelegate.h"

int main(int argc, char *argv[]){
    @autoreleasepool{
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MeetyAppDelegate class]));
    }
}
