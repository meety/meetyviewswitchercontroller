//
//  MeetyViewSwitcherController.m
//  MeetyViewSwitcherController
//
//  Created by 최건우 on 12. 9. 28..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetyViewSwitcherController.h"
#import "UIView+FrameAddition.h"
#import "UIImage+SizeAddition.h"
#import "UIScrollView+Paging.h"
#import "MeetyViewSwitcherBaseScrollView.h"

@interface MeetyViewSwitcherController ()<UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray* internalViewControllers;
@property (nonatomic, strong) UIScrollView* baseScrollView;
@property (nonatomic, strong) UIImageView* backgroundImageView;
@property (nonatomic) CGPoint lastOffset;

@end

@implementation MeetyViewSwitcherController
@dynamic viewControllers;
@dynamic numberOfPages;
@dynamic scrollView;
@dynamic backgroundImage;
@dynamic topViewController;
@synthesize contentInsets=_contentInsets;
@synthesize page=_page;
@synthesize portraitBackgroundImage=_portraitBackgroundImage;
@synthesize landscapeBackgroundImage=_landscapeBackgroundImage;
@synthesize internalViewControllers=_internalViewControllers;
@synthesize baseScrollView=_baseScrollView;
@synthesize backgroundImageView=_backgroundImageView;
@synthesize backgroundScrollView=_backgroundScrollView;
@synthesize lastOffset=_lastOffset;
@synthesize delegate=_delegate;

#pragma mark - Private methods

- (void)showViewController:(UIViewController*)viewController{
    if (viewController.view.superview != nil) {
        return;
    }
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:willShowViewController:)]) {
        [self.delegate viewSwitcherController:self willShowViewController:viewController];
    }
    [self.baseScrollView addSubview:viewController.view];
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:didShowViewController:)]) {
        [self.delegate viewSwitcherController:self didShowViewController:viewController];
    }
}

- (void)hideViewController:(UIViewController*)viewController{
    if (viewController.view.subviews == nil) {
        return;
    }
    [viewController.view removeFromSuperview];
}

- (CGPoint)offsetForPage:(NSUInteger)page withScrollViewFrame:(CGRect)frame{
    return CGPointMake(frame.size.width * page, 0);
}

- (CGPoint)offsetForPage:(NSUInteger)page{
    return [self offsetForPage:page withScrollViewFrame:self.baseScrollView.frame];
}

- (CGPoint)backgroundOffsetWithScrollOffset:(CGPoint)offset{
    if (self.backgroundImageView.width < self.backgroundScrollView.width){
        return CGPointZero;
    }
    CGFloat x =  (self.backgroundImageView.width - self.backgroundScrollView.width)/ ((self.numberOfPages - 1) * self.backgroundScrollView.width) * offset.x;
    if (x <= 0) {
        x = 0;
    }else if(x > self.backgroundImageView.width - self.backgroundScrollView.width){
        x = self.backgroundImageView.width - self.backgroundScrollView.width;
    }
    x = floorf(x);
    return CGPointMake(x, 0);
}

- (void)layoutBackgroundAnimated:(BOOL)animated{
    UIImage* image = nil;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)){
        image = self.portraitBackgroundImage;
    }else{
        image = self.landscapeBackgroundImage;
    }
    if (image == nil) {
        return;
    }
    CGFloat ratio = self.backgroundScrollView.height / image.height;
    self.backgroundImageView.image = image;
    self.backgroundImageView.height = self.backgroundScrollView.height;
    self.backgroundImageView.width = image.width * ratio;
    self.backgroundScrollView.contentSize = self.backgroundImageView.size;
}

- (void)layoutSubViewsWithScrollViewFrame:(CGRect)frame{
    CGFloat right = 0;
    for (NSUInteger i=0; i < self.internalViewControllers.count; i++) {
        UIViewController* vc = [self.internalViewControllers objectAtIndex:i];
        CGPoint origin = [self offsetForPage:i withScrollViewFrame:frame];
        CGSize size = frame.size;
        right = origin.x + size.width;
        
        origin.x += self.contentInsets.left;
        size.width -= self.contentInsets.right + self.contentInsets.left;
        origin.y += self.contentInsets.top;
        size.height -= self.contentInsets.bottom + self.contentInsets.top;
        CGRect subViewFrame;
        subViewFrame.origin = origin;
        subViewFrame.size = size;
        vc.view.frame = subViewFrame;
    }
    self.baseScrollView.contentSize = CGSizeMake(right, frame.size.height);
}

- (void)layoutSubViews{
    [self layoutSubViewsWithScrollViewFrame:self.baseScrollView.frame];
}

#pragma mark - Initializers

- (id)initWithFirstViewController:(UIViewController*)viewController{
    self = [super init];
    if (self) {
        // Init data
        self.contentInsets = UIEdgeInsetsZero;
        self.internalViewControllers = [NSMutableArray array];
        [self addViewController:viewController];
        self.page = 0;
    }
    return self;
}

#pragma mark - Public methods

- (void)addViewController:(UIViewController*)viewController{
    [self.internalViewControllers addObject:viewController];
    [self addChildViewController:viewController];
    if (self.isViewLoaded) {
        if (self.page == self.internalViewControllers.count - 1) {
            [self showViewController:viewController];
        }
        [self layoutSubViews];
    }
}

- (void)removeViewController:(UIViewController*)viewController{
    if (![self.internalViewControllers containsObject:viewController]) {
        return;
    }
    [self.internalViewControllers removeObject:viewController];
    [viewController removeFromParentViewController];
    if (self.isViewLoaded){
        [self hideViewController:viewController];
        [self layoutSubViews];
    }
}
- (UIViewController*)viewControllerAtPage:(NSUInteger)page{
    if (page < self.numberOfPages) {
        return [self.internalViewControllers objectAtIndex:page];
    }
    return nil;
}

#pragma mark - Getters and Setters

- (NSArray *)viewControllers{
    return [self.internalViewControllers copy];
}

- (NSUInteger)numberOfPages{
    return [self.internalViewControllers count];
}

- (UIScrollView *)scrollView{
    return self.baseScrollView;
}

- (UIImage *)backgroundImage{
    if (self.portraitBackgroundImage == nil) {
        return self.landscapeBackgroundImage;
    }
    return self.portraitBackgroundImage;
}

- (void)setBackgroundImage:(UIImage *)backgroundImage{
    self.portraitBackgroundImage = backgroundImage;
    self.landscapeBackgroundImage = backgroundImage;
}

- (void)setPage:(NSUInteger)page animated:(BOOL)animated{
    if (page >= self.numberOfPages) {
        return;
    }
    NSUInteger prevPage = _page;
    
    // Call delegate
    if (page != prevPage) {
        if ([self.delegate respondsToSelector:@selector(viewSwitcherController:willChangePageTo:)]) {
            [self.delegate viewSwitcherController:self willChangePageTo:page];
        }
    }
    
    // Change
    _page = page;
    CGPoint offset = [self offsetForPage:page];
    [self.baseScrollView setContentOffset:offset animated:animated];
    [self.backgroundScrollView setContentOffset:[self backgroundOffsetWithScrollOffset:offset] animated:animated];
    if (!animated) {
        UIViewController* destination = [self viewControllerAtPage:page];
        for (UIViewController* vc in self.internalViewControllers) {
            if (vc == destination) {
                [self showViewController:vc];
            } else{
                [self hideViewController:vc];
            }
        }
    }
    
    // Call delegate
    if (page != prevPage) {
        if([self.delegate respondsToSelector:@selector(viewSwitcherController:didChangePageFrom:)]){
            [self.delegate viewSwitcherController:self didChangePageFrom:prevPage];
        }
    }
}

- (void)setPage:(NSUInteger)page{
    [self setPage:page animated:NO];
}

- (void)setPortraitBackgroundImage:(UIImage *)portraitBackgroundImage{
    self->_portraitBackgroundImage = portraitBackgroundImage;
    [self layoutBackgroundAnimated:NO];
}

- (void)setLandscapeBackgroundImage:(UIImage *)landscapeBackgroundImage{
    self->_landscapeBackgroundImage = landscapeBackgroundImage;
    [self layoutBackgroundAnimated:NO];
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets{
    _contentInsets = contentInsets;
    [self layoutSubViews];
}

- (UIViewController*)topViewController{
    return [self.internalViewControllers objectAtIndex:self.page];
}

#pragma mark - Overrides

#pragma mark UIViewController overrides

- (NSUInteger)supportedInterfaceOrientations{
    return [self.topViewController supportedInterfaceOrientations];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return [self.topViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}

- (void)loadView{
    [super loadView];
    
    // Main view
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.autoresizesSubviews = YES;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    // Background scroll view
    self->_backgroundScrollView = [[MeetyViewSwitcherBaseScrollView alloc] initWithFrame:[self.view bounds]];
    self.backgroundScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.backgroundScrollView.backgroundColor = [UIColor clearColor];
    self.backgroundScrollView.opaque = YES;
    self.backgroundScrollView.userInteractionEnabled = NO;
    self.backgroundScrollView.showsHorizontalScrollIndicator = NO;
    self.backgroundScrollView.showsVerticalScrollIndicator = NO;
    self.backgroundScrollView.scrollsToTop = NO;
    [self.view addSubview:self.backgroundScrollView];
    
    // Background image view
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:[self.backgroundScrollView bounds]];
    self.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.backgroundScrollView addSubview:self.backgroundImageView];
    
    // Base scroll view
    self.baseScrollView = [[MeetyViewSwitcherBaseScrollView alloc] initWithFrame:[self.view bounds]];
    self.baseScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.baseScrollView.backgroundColor = [UIColor clearColor];
    self.baseScrollView.opaque = NO;
    self.baseScrollView.scrollEnabled = YES;
    self.baseScrollView.pagingEnabled = YES;
    self.baseScrollView.showsHorizontalScrollIndicator = NO;
    self.baseScrollView.showsVerticalScrollIndicator = NO;
    self.baseScrollView.alwaysBounceHorizontal = YES;
    self.baseScrollView.alwaysBounceVertical = NO;
    self.baseScrollView.delegate = self;
    self.baseScrollView.scrollsToTop = NO;
    [self.view addSubview:self.baseScrollView];
    
    for (NSUInteger i = 0; i < self.numberOfPages; i++) {
        if (i == self.page) {
            [self showViewController:[self.internalViewControllers objectAtIndex:i]];
        }
    }
    [self layoutSubViews];
    [self layoutBackgroundAnimated:NO];
    self.page = self.page;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self layoutSubViews];
    [self layoutBackgroundAnimated:NO];
    self.page = self.page;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // Interface changing support
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (void)viewDidUnload{
    [super viewDidUnload];
    self.baseScrollView = nil;
    self.backgroundImageView = nil;
    [self willChangeValueForKey:@"backgroundScrollView"];
    self->_backgroundScrollView = nil;
    [self didChangeValueForKey:@"backgroundScrollView"];
}
#endif

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

#pragma mark - Scroll view delegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.lastOffset = scrollView.contentOffset;
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:willBeginDragging:)]) {
        [self.delegate viewSwitcherController:self willBeginDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    BOOL goingRight = scrollView.contentOffset.x > self.lastOffset.x;
    NSUInteger prevPage, nextPage;
    
    // Move background
    self.backgroundScrollView.contentOffset = [self backgroundOffsetWithScrollOffset:scrollView.contentOffset];
    // Guess revealing page and last page
    if (goingRight) {
        prevPage = [scrollView horizontalPageForOffset:self.lastOffset];
        nextPage = prevPage + 1;
    }else{
        CGPoint offset = self.lastOffset;
        offset.x += scrollView.width;
        prevPage = [scrollView horizontalPageForOffset:offset];
        if (prevPage == 0) {
            nextPage = 0;
        }else{
            nextPage = prevPage - 1;
        }
    }
    if (prevPage == nextPage || scrollView.contentOffset.x == [scrollView offsetForHorizontalIndex:nextPage].x || scrollView.contentOffset.x < 0 || scrollView.contentOffset.x == self.lastOffset.x){
        return;
    }
    self.lastOffset = scrollView.contentOffset;
    
    if (goingRight) {
        if (nextPage < self.numberOfPages) {
            // Detach disappearing view
            UIViewController* leftController = [self viewControllerAtPage:prevPage - 1];
            [self hideViewController:leftController];
            
            // Attach appearing view
            UIViewController* rightController = [self viewControllerAtPage:nextPage];
            [self showViewController:rightController];
        }
    } else{
        // Detach disappearing view
        if (prevPage < self.numberOfPages - 1) {
            UIViewController* rightController = [self viewControllerAtPage:prevPage + 1];
            [self hideViewController:rightController];
        }
        
        // Attach appearing view
        UIViewController* leftController = [self viewControllerAtPage:nextPage];
        [self showViewController:leftController];
    }
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:didScroll:)]) {
        [self.delegate viewSwitcherController:self didScroll:scrollView];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    return NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSUInteger page = [scrollView currentHorizontalPage];
    self.page = page;
    UIViewController* currentViewController = [self viewControllerAtPage:page];
    for (UIViewController* vc in self.internalViewControllers) {
        if (vc != currentViewController) {
            [self hideViewController:vc];
        }
    }
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:didEndDecelerating:)]) {
        [self.delegate viewSwitcherController:self didEndDecelerating:scrollView];
    }
}

#pragma mark Redirection

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:willEndDragging:withVelocity:targetContentOffset:)]) {
        [self.delegate viewSwitcherController:self willEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:didEndDragging:willDecelerate:)]) {
        [self.delegate viewSwitcherController:self didEndDragging:scrollView willDecelerate:decelerate];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if ([self.delegate respondsToSelector:@selector(viewSwitcherController:didEndScrollingAnimation:)]) {
        [self.delegate viewSwitcherController:self didEndScrollingAnimation:scrollView];
    }
}
@end