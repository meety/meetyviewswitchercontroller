//
//  MeetyViewSwitcherController.h
//  MeetyViewSwitcherController
//
//  Created by 최건우 on 12. 9. 28..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+SwitcherAddition.h"
@class MeetyViewSwitcherController;
@protocol MeetyViewSwitcherControllerDelegate <NSObject>
@optional
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController willChangePageTo:(NSUInteger)page;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didChangePageFrom:(NSUInteger)page;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController willShowViewController:(UIViewController*)viewController;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didShowViewController:(UIViewController*)viewController;

// Scroll delegate
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didScroll:(UIScrollView *)scrollView;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController willBeginDragging:(UIScrollView *)scrollView;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController willEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController willBeginDecelerating:(UIScrollView *)scrollView;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didEndDecelerating:(UIScrollView *)scrollView;
- (void)viewSwitcherController:(MeetyViewSwitcherController*)viewSwitcherController didEndScrollingAnimation:(UIScrollView *)scrollView;
@end

@interface MeetyViewSwitcherController : UIViewController

@property (nonatomic) NSUInteger page;
@property (nonatomic, readonly) NSUInteger numberOfPages;
@property (nonatomic) UIEdgeInsets contentInsets;
@property (nonatomic, copy, readonly) NSArray* viewControllers;
@property (nonatomic, readonly) UIViewController* topViewController;
@property (nonatomic, readonly) UIScrollView* scrollView;
@property (nonatomic, readonly) UIScrollView* backgroundScrollView;
@property (nonatomic, strong) UIImage* portraitBackgroundImage;
@property (nonatomic, strong) UIImage* landscapeBackgroundImage;
@property (nonatomic, strong) UIImage* backgroundImage;
@property (nonatomic, weak) id<MeetyViewSwitcherControllerDelegate> delegate;

- (id)initWithFirstViewController:(UIViewController*)viewController;

- (void)addViewController:(UIViewController*)viewController;
- (void)removeViewController:(UIViewController*)viewController;
- (void)setPage:(NSUInteger)page animated:(BOOL)animated;
- (UIViewController*)viewControllerAtPage:(NSUInteger)page;

@end