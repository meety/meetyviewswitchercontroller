//
//  UIViewController+SwitcherAddition.h
//  MeetyViewSwitcherController
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MeetyViewSwitcherController;
@interface UIViewController (SwitcherAddition)

@property (nonatomic, readonly) MeetyViewSwitcherController* viewSwitcherController;

@end
