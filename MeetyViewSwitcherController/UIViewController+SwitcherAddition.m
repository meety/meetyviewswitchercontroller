//
//  UIViewController+SwitcherAddition.m
//  MeetyViewSwitcherController
//
//  Created by 최건우 on 12. 9. 29..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "UIViewController+SwitcherAddition.h"
#import "MeetyViewSwitcherController.h"

@implementation UIViewController (SwitcherAddition)
@dynamic viewSwitcherController;

- (MeetyViewSwitcherController *)viewSwitcherController{
    UIViewController* vc = self.parentViewController;
    if ([vc isKindOfClass:[MeetyViewSwitcherController class]]) {
        return (MeetyViewSwitcherController*)vc;
    }
    return nil;
}

@end
