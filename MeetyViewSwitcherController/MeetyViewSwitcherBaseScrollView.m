//
//  MeetyViewSwitcherBaseScrollView.m
//  MeetyViewSwitcherController
//
//  Created by 최건우 on 12. 10. 31..
//  Copyright (c) 2012년 최건우. All rights reserved.
//

#import "MeetyViewSwitcherBaseScrollView.h"

@implementation MeetyViewSwitcherBaseScrollView

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self setContentSize:CGSizeMake(self.contentSize.width, self.frame.size.height)];
}

@end
