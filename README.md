MeetyViewSwitcherController
===========================

Simple view switcher made by using iOS5's view container API.  

Introduction
-------------

**MeetyViewSwitcherController** is a kind of **UIViewController** which made by
using **iOS5**'s [View Container API](http://developer.apple.com/library/ios/#featuredarticles/ViewControllerPGforiPhoneOS/CreatingCustomContainerViewControllers/CreatingCustomContainerViewControllers.html).  

It is similar to **Android**'s [ViewSwitcher](http://developer.android.com/reference/android/widget/ViewSwitcher.html)

### Requirements ###

*   The iOS 5.1 SDK is needed to build. Maybe it can however run on iOS 5.0.  

### Download ###

You can download or clone it from [Bitbucket](https://bitbucket.org/meety/meetyviewswitchercontroller/).  

### Installation ###

This project compiles to a static library which you can link. 

This is guide to install **MeetyViewSwitcherController** into your project called
"PROJECT"

1.  Clone or download **MeetyViewSwitcherController**

2.  Delete .git & .gitignore if exists

3.  Delete Examples folder if you want

4.  Move the **MeetyViewSwitcherController** folder into the you PROJECT's folder

5.  From within Xcode, Open your PROJECT & add
"MeetyViewSwitcherController.xcodeproj"

6.  **MeetyViewSwitcherController** should appear in the Xcode Project Navigator

7.  Click on the "PROJECT" entry, Targets → PROJECT → Build Phases → Target
Dependencies → + “MeetyViewSwitcherController”

8.  Click on the "PROJECT" entry, Targets → PROJECT → Build Phases → Link Binary
with Libraries + "libMeetyViewSwitcherController.a"

9.  Click on the “PROJECT” entry, Targets → PROJECT → Build Settings → Search
Paths → User Header Search Paths → + “MeetyViewSwitcherController”'s path

10. Click on the “PROJECT” entry, Targets → PROJECT → Build Settings → 
Linking → Other Linker Flags → + “-ObjC”

11. It finished! Now you can use it

How To Use It
-------------

See example "BasicUse" in Examples folder. It describes well.  
